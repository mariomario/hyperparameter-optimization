library("FNN")
set.seed(888)

to <- read.table("together", header = F)#, stringsAsFactors = FALSE)
if(!table(to$V1 == to$V10)[1]) stop("holes and features not aligned!")

data <- to[,c(2:9,12)]
names(data) <- c(paste("F", 1:8, sep = ""), "IMBH") 

stratify <- function(labels, fraction)
{
    l <- length(labels)
    hosts <- (1:l)[labels == "Y"]
    nonhosts <- (1:l)[labels == "N"]
    selected <- c(sample(hosts, round(length(hosts)*fraction), replace = FALSE), sample(nonhosts, round(length(nonhosts)*fraction), replace = FALSE))
    return(selected)
}

simpleAUC <- function(labels, scores){
	labels <- labels[order(scores, decreasing=TRUE)]
	TPR <- cumsum(labels)/sum(labels)
        FPR <- cumsum(!labels)/sum(!labels)
        AUC <- 0.5*sum(diff(FPR)*(TPR[1:(length(TPR)-1)] + TPR[2:length(TPR)]))
        return(AUC)
	}

holdout_i <- stratify(data$IMBH, 0.2)
holdout <- data[holdout_i,]
dataset <- data[!(1:nrow(data) %in% holdout_i),]

hyperoptim_knn <- function(dataset, folds, knn.k)
{
    foldsize <- ceiling(nrow(dataset)/folds)
    splitvec <- rep(1:folds, foldsize) #make a vector with an equal number of 1, 2, ... 5
    splitvec <- splitvec[1:nrow(dataset)] #trim it to the length of the dataset (the number is not exactly equal anymore)
    splitvec <- sample(splitvec, length(splitvec), replace = F) #permute it

    #initializing arrays for prediction probability and true values
    truth <- character(0) #array of ground truths
    probs_knn <- numeric(0) #array of probabilities predicted by knn

    for (i in 1:folds) #n-fold cross-validation
    {
        # PREPARE TRAIN AND TEST
        trainvec <- splitvec != i #train on (n-1)/n of the dataset
        testvec <- splitvec == i #test on 1/n
        train <- dataset[trainvec,] 
        test <- dataset[testvec,]
        truth <- c(truth, test$IMBH) #store the actual value (IMBH yes/no)

        # K-NEAREST NEIGHBOR
        prediction_knn <- knn(train=train[,-ncol(train)], test=test[,-ncol(test)], cl=train$IMBH, prob=TRUE, k = knn.k)
        probs_knn <- c(probs_knn, 1 - attributes(prediction_knn)$prob)
    }

    truth <- as.numeric(as.character(truth))
    truth <- truth -1

    AUC <- simpleAUC(truth, probs_knn)
    return(AUC)
}

ks <- rep(1:50, 100) #rerunning resamples the cross validation split, so 10 repeats of CV
AUCs <- sapply(ks, function(k) hyperoptim_knn(dataset, 10, k))

pdf("knn_optim.pdf")
plot(ks, AUCs, pch = ".", xlab = "k", ylab = "AUC", col = "#00000030")
lines(c(-100, 100), c(0.5, 0.5))


