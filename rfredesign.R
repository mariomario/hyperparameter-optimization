rf <- read.table("rftables", header = TRUE)
pdf("rfredesign.pdf")
par(mfrow=c(4,1), oma = c(4,3,1,1), mar = c(0,0,0,0))
plot(log10(rf$trees), rf$AUCs_025, ylim = c(0.82, 1.0), xlab = "", ylab = "AUC", col = "#00C00030", pch = ".")
plot(log10(rf$trees), rf$AUCs_05, ylim = c(0.82, 1.0), xlab = "", ylab = "AUC", col = "#00903030", pch = ".")
plot(log10(rf$trees), rf$AUCs_075, ylim = c(0.82, 1.0), xlab = "", ylab = "AUC", col = "#00309030", pch = ".")
plot(log10(rf$trees), rf$AUCs_1, ylim = c(0.82, 1.0), xlab = "Log number of trees", ylab = "AUC", col = "#0000C030", pch = ".")
q()
